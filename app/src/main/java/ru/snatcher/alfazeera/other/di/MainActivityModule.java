package ru.snatcher.alfazeera.other.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.snatcher.alfazeera.ui.MainActivity;

@Module
public abstract class MainActivityModule {
	@ContributesAndroidInjector(modules = FragmentBuildersModule.class)
	abstract MainActivity contributeMainActivity();
}
