package ru.snatcher.alfazeera.data.repositories;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.text.Html;

import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.other.util.Constants;

public class SocialRepository {

	@Inject
	SocialRepository() {
	}

	public void login(int socialId, Activity activity) {
		if (socialId == Constants.VK) {
			loginVK(activity);
		} else if (socialId == Constants.FACEBOOK) {

		}
	}

	private void loginVK(Activity activity) {
		VKSdk.login(activity, VKScope.WALL);
	}

	public void shareToVK(News news, FragmentManager fragmentManager) {
		VKShareDialogBuilder builder = new VKShareDialogBuilder();
		builder.setText(Html.fromHtml(news.description));
		builder.setAttachmentLink(news.title, news.guid);
		builder.show(fragmentManager, "Share");
	}
}