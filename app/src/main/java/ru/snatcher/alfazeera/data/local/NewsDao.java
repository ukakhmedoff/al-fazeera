package ru.snatcher.alfazeera.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ru.snatcher.alfazeera.data.vo.News;

/**
 * Interface for database access for News related operations.
 */
@Dao
public interface NewsDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insert(News news);

	@Insert(onConflict = OnConflictStrategy.IGNORE)
	void insertAll(List<News> newsList);

	@Query("SELECT * FROM news WHERE title = :title")
	LiveData<News> findById(String title);

	@Query("SELECT * FROM news WHERE favourite = 1")
	LiveData<List<News>> loadFavourites();

	@Query("SELECT * FROM news")
	LiveData<List<News>> loadAll();
}