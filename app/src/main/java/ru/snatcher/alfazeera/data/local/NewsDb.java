package ru.snatcher.alfazeera.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.snatcher.alfazeera.data.vo.News;

/**
 * Main database description.
 */
@Database(entities = {News.class}, version = 2)
public abstract class NewsDb extends RoomDatabase {
	abstract public NewsDao newsDao();
}