package ru.snatcher.alfazeera.ui.fragments.splash;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.repositories.SocialRepository;

public class SplashFragmentViewModel extends ViewModel {

	private final SocialRepository mSocialRepository;

	@Inject
	SplashFragmentViewModel(SocialRepository socialRepository) {
		mSocialRepository = socialRepository;
	}

	void login(int socialId, Activity activity) {
		mSocialRepository.login(socialId, activity);
	}
}
