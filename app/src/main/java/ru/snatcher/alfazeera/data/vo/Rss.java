package ru.snatcher.alfazeera.data.vo;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class Rss {
	@Element(name = "channel")
	public Channel mChannel;

	private Rss(Channel channel) {
		mChannel = channel;
	}

	private Rss() {
	}
}
