package ru.snatcher.alfazeera.ui.binding;

import android.databinding.BindingAdapter;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.widget.TextView;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.vo.Status;
import ru.snatcher.alfazeera.ui.common.RetryCallback;

/**
 * Binding adapters that work with a fragment instance.
 */
public class FragmentBindingAdapters {

	@Inject
	FragmentBindingAdapters() {
	}

	@BindingAdapter("refresh_listener")
	public void bindRefreshListener(SwipeRefreshLayout view, RetryCallback retryCallback) {
		view.setOnRefreshListener(retryCallback::retry);
	}

	@BindingAdapter("refreshing")
	public void bindRefreshing(SwipeRefreshLayout view, Status status) {
		view.setRefreshing(status == Status.LOADING);
	}

	@BindingAdapter("textHtml")
	public void bindTextHtml(TextView view, String text) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
			if (text != null && text.length() > 0) {
				view.setText(Html.fromHtml(text));
			}
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			if (text != null && text.length() > 0) {
				view.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
			}
		}
	}
}