package ru.snatcher.alfazeera.data.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.remote.AlfaService;
import ru.snatcher.alfazeera.data.remote.ApiResponse;
import ru.snatcher.alfazeera.data.local.NewsDao;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.data.vo.Resource;
import ru.snatcher.alfazeera.data.vo.Rss;
import ru.snatcher.alfazeera.other.AppExecutors;
import ru.snatcher.alfazeera.other.util.AbsentLiveData;

public class NewsRepository {
	private final AppExecutors mAppExecutors;
	private final AlfaService mService;
	private final NewsDao mDao;

	@Inject
	NewsRepository(AppExecutors appExecutors, final AlfaService service, final NewsDao dao) {
		mService = service;
		mDao = dao;
		mAppExecutors = appExecutors;
	}

	public LiveData<Resource<List<News>>> getNewsList(boolean favourite, boolean fetch) {
		return new NetworkBoundResource<List<News>, Rss>(mAppExecutors) {

			@Override
			protected void saveCallResult(@NonNull Rss item) {
				mDao.insertAll(item.mChannel.mNews);
			}

			@Override
			protected boolean shouldFetch(@Nullable List<News> data) {
				return fetch;
			}

			@NonNull
			@Override
			protected LiveData<List<News>> loadFromDb() {
				if (favourite) {
					return mDao.loadFavourites();
				} else {
					return mDao.loadAll();
				}
			}

			@NonNull
			@Override
			protected LiveData<ApiResponse<Rss>> createCall() {
				return mService.getNewsList();
			}
		}.asLiveData();
	}

	public final LiveData<Resource<News>> getNews(String news) {
		return new NetworkBoundResource<News, News>(mAppExecutors) {

			@Override
			protected void saveCallResult(@NonNull News item) {
				mDao.insert(item);
			}

			@Override
			protected boolean shouldFetch(@Nullable News data) {
				return false;
			}

			@NonNull
			@Override
			protected LiveData<News> loadFromDb() {
				return mDao.findById(news);
			}

			@NonNull
			@Override
			protected LiveData<ApiResponse<News>> createCall() {
				return AbsentLiveData.create();
			}
		}.asLiveData();
	}

	public final void saveNews(News news) {
		mDao.insert(news);
	}
}