package ru.snatcher.alfazeera.data.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item", strict = false)
@Entity
public class News {

	public boolean favourite = false;
	@Element(name = "title")
	@PrimaryKey
	public String title;
	@Element(name = "link")
	public String link;
	@Element(name = "description")
	public String description;
	@Element(name = "pubDate")
	public String pubDate;
	@Element(name = "guid")
	public String guid;

	public News(String title, String link, String description, String pubDate, String guid) {
		this.title = title;
		this.link = link;
		this.description = description;
		this.pubDate = pubDate;
		this.guid = guid;
	}

	private News(boolean favourite, String title, String link, String description,
			String pubDate, String guid) {
		this.favourite = favourite;
		this.title = title;
		this.link = link;
		this.description = description;
		this.pubDate = pubDate;
		this.guid = guid;
	}

	private News() {
	}
}