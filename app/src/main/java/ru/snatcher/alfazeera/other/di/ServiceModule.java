package ru.snatcher.alfazeera.other.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.snatcher.alfazeera.data.services.UpdateDataService;

@Module
abstract class ServiceModule {

	@ContributesAndroidInjector
	abstract UpdateDataService contributeUpdateDataService();
}
