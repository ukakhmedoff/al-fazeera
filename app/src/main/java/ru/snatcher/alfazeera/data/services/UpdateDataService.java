package ru.snatcher.alfazeera.data.services;

import static ru.snatcher.alfazeera.other.util.Constants.IS_FAVOURITE;
import static ru.snatcher.alfazeera.other.util.Constants.SHOULD_FETCH;

import android.app.IntentService;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import ru.snatcher.alfazeera.data.repositories.NewsRepository;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class UpdateDataService extends LifecycleService {

	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
	@Inject
	public NewsRepository mRepository;

	@Override
	public Lifecycle getLifecycle() {
		return mRegistry;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		AndroidInjection.inject(this);
		mRepository.getNewsList(!IS_FAVOURITE, SHOULD_FETCH);
	}
}