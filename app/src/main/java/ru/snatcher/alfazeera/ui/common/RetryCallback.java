package ru.snatcher.alfazeera.ui.common;

/**
 * Generic interface for retry buttons.
 */
public interface RetryCallback {
	void retry();
}
