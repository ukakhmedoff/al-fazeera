package ru.snatcher.alfazeera.other;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.vk.sdk.VKSdk;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import ru.snatcher.alfazeera.other.di.AppInjector;

public class App extends Application implements HasActivityInjector, HasServiceInjector {
	@Inject
	DispatchingAndroidInjector<Activity> dispatchingActivityInjector;
	@Inject
	DispatchingAndroidInjector<Service> dispatchingServiceInjector;

	@Override
	public void onCreate() {
		super.onCreate();
		AppInjector.init(this);
		VKSdk.initialize(this);
	}

	@Override
	public DispatchingAndroidInjector<Activity> activityInjector() {
		return dispatchingActivityInjector;
	}

	@Override
	public DispatchingAndroidInjector<Service> serviceInjector() {
		return dispatchingServiceInjector;
	}
}
