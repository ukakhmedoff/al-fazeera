package ru.snatcher.alfazeera.ui.fragments.news;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;

import javax.inject.Inject;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.data.vo.Resource;
import ru.snatcher.alfazeera.databinding.FragmentNewsPageBinding;
import ru.snatcher.alfazeera.other.di.Injectable;
import ru.snatcher.alfazeera.other.util.AutoClearedValue;
import ru.snatcher.alfazeera.other.util.Constants;
import ru.snatcher.alfazeera.ui.binding.FragmentDataBindingComponent;

public class NewsPageFragment extends LifecycleFragment implements Injectable {
	private static final String NEWS_TITLE_KEY = "news_title";

	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
	@Inject
	ViewModelProvider.Factory mFactory;
	android.databinding.DataBindingComponent mComponent = new FragmentDataBindingComponent(this);
	private CallbackManager callbackManager;

	private NewsFragmentViewModel mViewModel;
	private AutoClearedValue<FragmentNewsPageBinding> mBinding;

	public static NewsPageFragment create(String news) {
		Bundle args = new Bundle();
		args.putString(NEWS_TITLE_KEY, news);
		NewsPageFragment fragment = new NewsPageFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		callbackManager = CallbackManager.Factory.create();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		FragmentNewsPageBinding binding = DataBindingUtil.inflate(inflater,
				R.layout.fragment_news_page, container, false, mComponent);
		mBinding = new AutoClearedValue<>(this, binding);
		return binding.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mViewModel = ViewModelProviders.of(this, mFactory).get(NewsFragmentViewModel.class);
		showUi();
	}

	private void showUi() {
		Bundle args = getArguments();

		mViewModel.getNews(args.getString(NEWS_TITLE_KEY)).observe(this, this::setData);
	}

	private void setData(Resource<News> resource) {
		if (resource != null && mBinding != null && mBinding.get() != null
				&& resource.data != null) {
			mBinding.get().setNews(resource.data);
			setClickListeners(resource);
		}
	}

	private void setClickListeners(Resource<News> resource) {
		ShareButton shareButton = mBinding.get().facebookShareButton;
		ShareLinkContent content = new ShareLinkContent.Builder()
				.setQuote(resource.data.title)
				.setContentUrl(Uri.parse(resource.data.guid))
				.build();
		shareButton.setShareContent(content);
		mBinding.get().vkShareButton.setOnClickListener(
				v -> mViewModel.shareToSocial(Constants.VK, mBinding.get().getNews(),
						getChildFragmentManager()));
		mBinding.get().imageView.setOnClickListener((view) -> {
			resource.data.favourite = !resource.data.favourite;
			mViewModel.saveNews(resource.data);
		});
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public LifecycleRegistry getLifecycle() {
		return mRegistry;
	}
}