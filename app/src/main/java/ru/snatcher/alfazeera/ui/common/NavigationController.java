package ru.snatcher.alfazeera.ui.common;

import static ru.snatcher.alfazeera.other.util.Constants.IS_FAVOURITE;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import javax.inject.Inject;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.other.util.Constants;
import ru.snatcher.alfazeera.ui.MainActivity;
import ru.snatcher.alfazeera.ui.fragments.feed.FeedFragment;
import ru.snatcher.alfazeera.ui.fragments.info.InfoFragment;
import ru.snatcher.alfazeera.ui.fragments.news.NewsFragment;
import ru.snatcher.alfazeera.ui.fragments.splash.SplashFragment;

public class NavigationController {
	private final int mContainerId;
	private final FragmentManager mManager;
	private FragmentTransaction mTransaction;
	private Fragment mFragment;

	@Inject
	public NavigationController(MainActivity mainActivity) {
		mContainerId = R.id.navigation_view_container;
		mManager = mainActivity.getSupportFragmentManager();
	}

	public void showFragment(int fragmentId) {
		if (fragmentId == Constants.FRAGMENT_INFO_ID) {
			navigateToInfo();
		} else if (fragmentId == Constants.FRAGMENT_FEED_ID) {
			navigateToFeed(!IS_FAVOURITE);
		} else if (fragmentId == Constants.FRAGMENT_FAVOURITE_ID) {
			navigateToFeed(IS_FAVOURITE);
		} else if (fragmentId == Constants.FRAGMENT_SPLASH_ID) {
			navigateToSplash();
		}
	}

	private void beginTransaction() {
		mTransaction = mManager.beginTransaction();
	}

	private void replaceFragment(Fragment fragment) {
		beginTransaction();
		mTransaction.replace(mContainerId, fragment, fragment.getTag());
	}

	private void addToBackStack() {
		mTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
		mTransaction.addToBackStack(null);
		commitTransaction();
	}

	private void commitTransaction() {
		mTransaction.commitAllowingStateLoss();
	}

	public int getBackStackCount() {
		return mManager.getBackStackEntryCount();
	}

	public void back() {
		mManager.popBackStack();
	}

	private void navigateToFeed(boolean favourite) {
		mFragment = FeedFragment.create(favourite, !favourite);
		replaceFragment(mFragment);
		commitTransaction();
	}

	public void navigateToNews(News news, boolean favourite, int i) {
		mFragment = NewsFragment.create(news, favourite, i);
		replaceFragment(mFragment);
		addToBackStack();
	}

	private void navigateToSplash() {
		mFragment = new SplashFragment();
		replaceFragment(mFragment);
		commitTransaction();
	}

	private void navigateToInfo() {
		mFragment = new InfoFragment();
		replaceFragment(mFragment);
		commitTransaction();
	}

	public Fragment getFragment() {
		return mFragment;
	}
}