package ru.snatcher.alfazeera.ui.fragments.splash;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import javax.inject.Inject;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.databinding.FragmentSplashBinding;
import ru.snatcher.alfazeera.other.di.Injectable;
import ru.snatcher.alfazeera.other.util.AutoClearedValue;
import ru.snatcher.alfazeera.other.util.Constants;
import ru.snatcher.alfazeera.ui.binding.FragmentDataBindingComponent;
import ru.snatcher.alfazeera.ui.common.NavigationController;

public class SplashFragment extends LifecycleFragment implements Injectable {

	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
	@Inject
	ViewModelProvider.Factory mFactory;
	android.databinding.DataBindingComponent mComponent = new FragmentDataBindingComponent(this);

	@Inject
	NavigationController mNavigationController;

	private AutoClearedValue<FragmentSplashBinding> mBinding;
	private CallbackManager callbackManager;
	private SplashFragmentViewModel mViewModel;
	private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
		@Override
		public void onSuccess(LoginResult loginResult) {
			mNavigationController.showFragment(Constants.FRAGMENT_FEED_ID);
		}

		@Override
		public void onCancel() {
		}

		@Override
		public void onError(FacebookException e) {
		}
	};

	@Override
	public LifecycleRegistry getLifecycle() {
		return mRegistry;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		callbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(callbackManager, callback);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		FragmentSplashBinding binding = DataBindingUtil.inflate(inflater,
				R.layout.fragment_splash, container, false, mComponent);
		mBinding = new AutoClearedValue<>(this, binding);
		return mBinding.get().getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mViewModel = ViewModelProviders.of(this, mFactory).get(SplashFragmentViewModel.class);

		showUi();
	}

	private void showUi() {
		initFacebookLoginButton();
		initVkLoginButton();
	}

	private void initVkLoginButton() {
		mBinding.get().loginButtonVk
				.setOnClickListener(v -> mViewModel.login(Constants.VK, getActivity()));
	}

	private void initFacebookLoginButton() {
		LoginButton loginButton = mBinding.get().loginButtonFacebook;
		loginButton.setReadPermissions("email");
		loginButton.setFragment(this);
		loginButton.registerCallback(callbackManager, callback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
}