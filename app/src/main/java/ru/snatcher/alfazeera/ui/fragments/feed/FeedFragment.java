package ru.snatcher.alfazeera.ui.fragments.feed;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.data.vo.Status;
import ru.snatcher.alfazeera.databinding.FragmentFeedBinding;
import ru.snatcher.alfazeera.other.di.Injectable;
import ru.snatcher.alfazeera.other.util.AutoClearedValue;
import ru.snatcher.alfazeera.ui.binding.FragmentDataBindingComponent;
import ru.snatcher.alfazeera.ui.common.FeedListAdapter;
import ru.snatcher.alfazeera.ui.common.NavigationController;

public class FeedFragment extends LifecycleFragment implements Injectable {
	public static final String FEED_FAVOURITE_KEY = "feed_favourite";
	public static final String FEED_SHOULD_FETCH_KEY = "feed_should_fetch";
	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

	@Inject
	NavigationController mController;

	@Inject
	ViewModelProvider.Factory mFactory;
	DataBindingComponent mComponent = new FragmentDataBindingComponent(this);

	private FeedFragmentViewModel mViewModel;
	private AutoClearedValue<FeedListAdapter> mAdapter;
	private AutoClearedValue<FragmentFeedBinding> mBinding;

	public static FeedFragment create(boolean favourite, boolean fetch) {
		Bundle args = new Bundle();
		args.putBoolean(FEED_FAVOURITE_KEY, favourite);
		args.putBoolean(FEED_SHOULD_FETCH_KEY, fetch);

		FeedFragment fragment = new FeedFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public LifecycleRegistry getLifecycle() {
		return mRegistry;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		FragmentFeedBinding binding = DataBindingUtil
				.inflate(inflater, R.layout.fragment_feed, container, false, mComponent);
		mBinding = new AutoClearedValue<>(this, binding);

		return binding.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		mViewModel = ViewModelProviders.of(this, mFactory).get(FeedFragmentViewModel.class);
		mBinding.get().setRetryCallback(this::initFeed);
		initRecycler();
		super.onActivityCreated(savedInstanceState);
	}

	private void initRecycler() {
		FeedListAdapter adapter = new FeedListAdapter(mComponent,
				(news, favourite, i) -> mController.navigateToNews(news, favourite, i),
				getArguments().getBoolean(FEED_FAVOURITE_KEY));
		RecyclerView recycler = mBinding.get().recycler;
		recycler.setAdapter(adapter);
		mAdapter = new AutoClearedValue<>(this, adapter);
		initFeed();
	}

	private void initFeed() {
		Bundle args = getArguments();
		mViewModel.getNewsList(args.getBoolean(FEED_FAVOURITE_KEY),
				args.getBoolean(FEED_SHOULD_FETCH_KEY)).observe(this, resource -> {
			if (resource != null) {
				mBinding.get().setResource(resource);
				if (resource.status == Status.SUCCESS) {
					mAdapter.get().add(resource.data);
				}
				mBinding.get().executePendingBindings();
			}
		});
	}
}