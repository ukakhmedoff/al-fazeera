package ru.snatcher.alfazeera.ui.binding;

import android.databinding.DataBindingComponent;
import android.support.v4.app.Fragment;

/**
 * A Data Binding Component implementation for fragments.
 */
public class FragmentDataBindingComponent implements DataBindingComponent {
	private final FragmentBindingAdapters adapter;

	public FragmentDataBindingComponent(Fragment fragment) {
		adapter = new FragmentBindingAdapters();
	}

	@Override
	public FragmentBindingAdapters getFragmentBindingAdapters() {
		return adapter;
	}
}