package ru.snatcher.alfazeera.other.di;

import javax.inject.Inject;

import dagger.Subcomponent;
import ru.snatcher.alfazeera.ui.fragments.feed.FeedFragmentViewModel;
import ru.snatcher.alfazeera.ui.fragments.news.NewsFragmentViewModel;
import ru.snatcher.alfazeera.ui.fragments.splash.SplashFragmentViewModel;
import ru.snatcher.alfazeera.viewmodel.AlfaViewModelFactory;

/**
 * A sub component to create ViewModels. It is called by the
 * {@link AlfaViewModelFactory}. Using this component allows
 * ViewModels to define {@link Inject} constructors.
 */
@Subcomponent
public interface ViewModelSubComponent {
	FeedFragmentViewModel mainFragmentViewModel();

	NewsFragmentViewModel newsFragmentViewModel();

	SplashFragmentViewModel splashFragmentViewModel();

	@Subcomponent.Builder
	interface Builder {
		ViewModelSubComponent build();
	}
}