package ru.snatcher.alfazeera.data.vo;

import static ru.snatcher.alfazeera.data.vo.Status.ERROR;
import static ru.snatcher.alfazeera.data.vo.Status.LOADING;
import static ru.snatcher.alfazeera.data.vo.Status.SUCCESS;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * A generic class that holds a value with its loading status.
 */
public class Resource<T> {

	@NonNull
	public final Status status;
	@Nullable
	public final T data;

	private Resource(@NonNull Status status, @Nullable T data) {
		this.status = status;
		this.data = data;
	}

	public static <T> Resource<T> success(@Nullable T data) {
		return new Resource<>(SUCCESS, data);
	}

	public static <T> Resource<T> error(@Nullable T data) {
		return new Resource<>(ERROR, data);
	}

	public static <T> Resource<T> loading(@Nullable T data) {
		return new Resource<>(LOADING, data);
	}
}
