package ru.snatcher.alfazeera.data.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import ru.snatcher.alfazeera.data.remote.ApiResponse;
import ru.snatcher.alfazeera.data.vo.Resource;
import ru.snatcher.alfazeera.other.AppExecutors;

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 * <p>
 * You can read more about it in the <a href="https://developer.android.com/arch">Architecture
 * Guide</a>.
 *
 * @param <ResultType>
 * @param <RequestType>
 */
public abstract class NetworkBoundResource<ResultType, RequestType> {
	private final AppExecutors appExecutors;

	private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

	@MainThread
	NetworkBoundResource(AppExecutors appExecutors) {
		this.appExecutors = appExecutors;
		result.setValue(Resource.loading(null));

		LiveData<ResultType> dbSource = loadFromDb();

		result.addSource(dbSource, data -> {
			result.removeSource(dbSource);
			if (shouldFetch(data)) {
				fetchFromNetwork(dbSource);
			} else {
				result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
			}
		});
	}

	private void fetchFromNetwork(final LiveData<ResultType> dbSource) {
		LiveData<ApiResponse<RequestType>> apiResponse = createCall();
		// we re-attach dbSource as a new source, it will dispatch its latest value quickly
		result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
		result.addSource(apiResponse, response -> {
			result.removeSource(apiResponse);
			result.removeSource(dbSource);
			//noinspection ConstantConditions
			if (response.isSuccessful()) {
				appExecutors.diskIO().execute(() -> {
					saveCallResult(processResponse(response));
					appExecutors.mainThread().execute(() ->
							result.addSource(loadFromDb(),
									newData -> result.setValue(Resource.success(newData)))
					);
				});
			} else {
				onFetchFailed();
				result.addSource(dbSource, newData -> result.setValue(Resource.error(newData)));
			}
		});
	}

	private void onFetchFailed() {
	}

	LiveData<Resource<ResultType>> asLiveData() {
		return result;
	}

	@WorkerThread
	private RequestType processResponse(ApiResponse<RequestType> response) {
		return response.body;
	}

	@WorkerThread
	protected abstract void saveCallResult(@NonNull RequestType item);

	@MainThread
	protected abstract boolean shouldFetch(@Nullable ResultType data);

	@NonNull
	@MainThread
	protected abstract LiveData<ResultType> loadFromDb();

	@NonNull
	@MainThread
	protected abstract LiveData<ApiResponse<RequestType>> createCall();
}