package ru.snatcher.alfazeera.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.v4.util.ArrayMap;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.snatcher.alfazeera.other.di.ViewModelSubComponent;
import ru.snatcher.alfazeera.ui.fragments.feed.FeedFragmentViewModel;
import ru.snatcher.alfazeera.ui.fragments.news.NewsFragmentViewModel;
import ru.snatcher.alfazeera.ui.fragments.splash.SplashFragmentViewModel;

@Singleton
public class AlfaViewModelFactory implements ViewModelProvider.Factory {
	private final ArrayMap<Class, Callable<? extends ViewModel>> creators;

	@Inject
	public AlfaViewModelFactory(ViewModelSubComponent subComponent) {
		creators = new ArrayMap<>();
		// we cannot inject view models directly because they won't be bound to the owner's
		// view model scope.
		creators.put(NewsFragmentViewModel.class, subComponent::newsFragmentViewModel);
		creators.put(NewsFragmentViewModel.class, subComponent::newsFragmentViewModel);
		creators.put(FeedFragmentViewModel.class, subComponent::mainFragmentViewModel);
		creators.put(SplashFragmentViewModel.class, subComponent::splashFragmentViewModel);
	}

	@Override
	public <T extends ViewModel> T create(Class<T> modelClass) {
		Callable<? extends ViewModel> creator = creators.get(modelClass);
		if (creator == null) {
			for (Map.Entry<Class, Callable<? extends ViewModel>> entry : creators.entrySet()) {
				if (modelClass.isAssignableFrom(entry.getKey())) {
					creator = entry.getValue();
					break;
				}
			}
		}
		if (creator == null) {
			throw new IllegalArgumentException("unknown model class " + modelClass);
		}
		try {
			return (T) creator.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}