package ru.snatcher.alfazeera.other.di;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import ru.snatcher.alfazeera.data.local.NewsDao;
import ru.snatcher.alfazeera.data.local.NewsDb;
import ru.snatcher.alfazeera.data.remote.AlfaService;
import ru.snatcher.alfazeera.other.util.LiveDataCallAdapterFactory;
import ru.snatcher.alfazeera.viewmodel.AlfaViewModelFactory;

@Module(subcomponents = ViewModelSubComponent.class)
class AppModule {

	@Singleton
	@Provides
	NewsDb provideDb(Application app) {
		return Room.databaseBuilder(app, NewsDb.class,
				"news.local").allowMainThreadQueries().build();
	}

	@Singleton
	@Provides
	NewsDao provideNewsDao(NewsDb db) {
		return db.newsDao();
	}

	@Singleton
	@Provides
	AlfaService provideNewsService() {
		return new Retrofit.Builder()
				.baseUrl("https://alfabank.ru/")
				.addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
				.addCallAdapterFactory(new LiveDataCallAdapterFactory())
				.build()
				.create(AlfaService.class);
	}

	@Singleton
	@Provides
	ViewModelProvider.Factory provideViewModelFactory(
			ViewModelSubComponent.Builder viewModelSubComponent) {
		return new AlfaViewModelFactory(viewModelSubComponent.build());
	}
}