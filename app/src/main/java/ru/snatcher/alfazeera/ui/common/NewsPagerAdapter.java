package ru.snatcher.alfazeera.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.ui.fragments.news.NewsPageFragment;

public class NewsPagerAdapter extends FragmentPagerAdapter {

	private List<News> mNewsList;

	public NewsPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	public void setNewsList(List<News> newsList) {
		mNewsList = newsList;
		notifyDataSetChanged();
	}

	@Override
	public Fragment getItem(int position) {
		return NewsPageFragment.create(mNewsList.get(position).title);
	}

	@Override
	public int getCount() {
		return mNewsList == null ? 0 : mNewsList.size();
	}
}