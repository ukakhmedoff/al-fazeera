package ru.snatcher.alfazeera.other.util;

public class Constants {

	public static final int FRAGMENT_FEED_ID = 0;
	public static final int FRAGMENT_FAVOURITE_ID = 1;
	public static final int FRAGMENT_INFO_ID = 2;
	public static final int FRAGMENT_SPLASH_ID = 3;

	public static final boolean IS_FAVOURITE = true;
	public static final boolean SHOULD_FETCH = true;

	public static final int VK = 0;
	public static final int FACEBOOK = 1;
}