package ru.snatcher.alfazeera.other.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import ru.snatcher.alfazeera.other.App;

@Singleton
@Component(modules = {
		AndroidInjectionModule.class,
		AppModule.class,
		MainActivityModule.class,
		ServiceModule.class
})
public interface AppComponent {
	void inject(App app);

	@Component.Builder
	interface Builder {
		@BindsInstance
		Builder application(Application application);

		AppComponent build();
	}
}
