package ru.snatcher.alfazeera.ui.common;

import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A generic RecyclerView adapter that uses Data Binding.
 *
 * @param <T> Type of the items in the list
 * @param <V> The of the ViewDataBinding
 */
public abstract class DataBoundListAdapter<T, V extends ViewDataBinding>
		extends RecyclerView.Adapter<DataBoundViewHolder<V>> {

	@Nullable
	public List<T> items = new ArrayList<>();

	@Override
	public final DataBoundViewHolder<V> onCreateViewHolder(ViewGroup parent, int viewType) {
		V binding = createBinding(parent);
		return new DataBoundViewHolder<>(binding);
	}

	protected abstract V createBinding(ViewGroup parent);

	@Override
	public final void onBindViewHolder(DataBoundViewHolder<V> holder, int position) {
		//noinspection ConstantConditions
		bind(holder.binding, items.get(position));
		holder.binding.executePendingBindings();
	}

	public void add(List<T> add) {
		items = add;
		notifyDataSetChanged();
	}

	protected abstract void bind(V binding, T item);

	@Override
	public int getItemCount() {
		return items == null ? 0 : items.size();
	}
}