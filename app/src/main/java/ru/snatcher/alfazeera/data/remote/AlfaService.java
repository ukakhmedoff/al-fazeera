package ru.snatcher.alfazeera.data.remote;

import android.arch.lifecycle.LiveData;

import retrofit2.http.GET;
import ru.snatcher.alfazeera.data.vo.Rss;

/**
 * REST API access points
 */
public interface AlfaService {

	@GET("_/rss/_rss.html?subtype=1&category=2&city=21")
	LiveData<ApiResponse<Rss>> getNewsList();
}