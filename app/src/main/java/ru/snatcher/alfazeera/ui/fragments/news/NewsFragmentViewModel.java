package ru.snatcher.alfazeera.ui.fragments.news;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.FragmentManager;

import java.util.List;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.repositories.NewsRepository;
import ru.snatcher.alfazeera.data.repositories.SocialRepository;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.data.vo.Resource;
import ru.snatcher.alfazeera.other.util.Constants;

public class NewsFragmentViewModel extends ViewModel {

	@VisibleForTesting
	private final MutableLiveData<String> mNews;

	private final NewsRepository mNewsRepository;
	private final SocialRepository mSocialRepository;

	@Inject
	NewsFragmentViewModel(NewsRepository newsRepository,
			SocialRepository socialRepository) {
		mSocialRepository = socialRepository;
		mNews = new MutableLiveData<>();
		mNewsRepository = newsRepository;
	}

	LiveData<Resource<News>> getNews(String news) {
		mNews.setValue(news);
		return Transformations.switchMap(mNews, mNewsRepository::getNews);
	}

	void saveNews(News news) {
		mNewsRepository.saveNews(news);
	}

	LiveData<Resource<List<News>>> getNewsList(boolean favourite) {
		return mNewsRepository.getNewsList(favourite, false);
	}

	void shareToSocial(int socialId, News news, FragmentManager fragmentManager) {
		if (socialId == Constants.VK) {
			shareToVK(news, fragmentManager);
		}
	}


	private void shareToVK(News news, FragmentManager fragmentManager) {
		mSocialRepository.shareToVK(news, fragmentManager);
	}
}
