package ru.snatcher.alfazeera.other.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.snatcher.alfazeera.ui.fragments.feed.FeedFragment;
import ru.snatcher.alfazeera.ui.fragments.news.NewsFragment;
import ru.snatcher.alfazeera.ui.fragments.news.NewsPageFragment;
import ru.snatcher.alfazeera.ui.fragments.splash.SplashFragment;

@Module
abstract class FragmentBuildersModule {
	@ContributesAndroidInjector
	abstract FeedFragment contributeMainFragment();

	@ContributesAndroidInjector
	abstract NewsFragment contributeNewsFragment();

	@ContributesAndroidInjector
	abstract NewsPageFragment contributeNewsPageFragment();

	@ContributesAndroidInjector
	abstract SplashFragment contributeSplashFragment();
}
