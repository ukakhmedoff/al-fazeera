package ru.snatcher.alfazeera.ui.common;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.databinding.FeedListItemBinding;

public class FeedListAdapter extends DataBoundListAdapter<News, FeedListItemBinding> {

	private final DataBindingComponent mComponent;
	private final NewsClickCallback mCallback;
	private final boolean favourite;

	public FeedListAdapter(DataBindingComponent component, NewsClickCallback callback,
			boolean favourite) {
		this.mComponent = component;
		this.mCallback = callback;
		this.favourite = favourite;
	}

	@Override
	protected FeedListItemBinding createBinding(final ViewGroup parent) {
		FeedListItemBinding binding = DataBindingUtil.inflate(
				LayoutInflater.from(parent.getContext()), R.layout.feed_list_item, parent,
				false, mComponent);

		binding.getRoot().setOnClickListener(v -> {
			News news = binding.getNews();
			if (news != null && mCallback != null) {
				for (int i = 0; i < items.size(); i++) {
					if (news == items.get(i)) {
						mCallback.onClick(news, favourite, i);
					}
				}
			}
		});
		return binding;
	}

	@Override
	protected void bind(FeedListItemBinding binding, News item) {
		binding.setNews(item);
	}

	public interface NewsClickCallback {
		void onClick(News news, boolean favourite, int i);
	}
}