package ru.snatcher.alfazeera.data.vo;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false)
public class Channel {

	@ElementList(name = "item", inline = true)
	public List<News> mNews;

	private Channel(List<News> news) {
		mNews = news;
	}

	private Channel() {
	}
}
