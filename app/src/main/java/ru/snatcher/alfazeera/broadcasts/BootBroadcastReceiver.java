package ru.snatcher.alfazeera.broadcasts;

import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.snatcher.alfazeera.data.services.UpdateDataService;

public class BootBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("alfazeeraLog", "onReceive: boot");
		Intent startServiceIntent = new Intent(context, UpdateDataService.class);
		startWakefulService(context, startServiceIntent);
	}
}