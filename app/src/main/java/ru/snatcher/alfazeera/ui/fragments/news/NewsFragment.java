package ru.snatcher.alfazeera.ui.fragments.news;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.databinding.FragmentNewsBinding;
import ru.snatcher.alfazeera.other.di.Injectable;
import ru.snatcher.alfazeera.other.util.AutoClearedValue;
import ru.snatcher.alfazeera.ui.binding.FragmentDataBindingComponent;
import ru.snatcher.alfazeera.ui.common.NewsPagerAdapter;

public class NewsFragment extends LifecycleFragment implements Injectable {
	private static final String NEWS_TITLE_KEY = "news_title";
	private static final String NEWS_FAVOURITE_KEY = "news_favourite";
	private static final String NEWS_INT_KEY = "news_int";

	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
	@Inject
	ViewModelProvider.Factory mFactory;
	DataBindingComponent mComponent = new FragmentDataBindingComponent(this);

	private NewsFragmentViewModel mViewModel;
	private AutoClearedValue<FragmentNewsBinding> mBinding;

	public static NewsFragment create(News news, boolean favourite, int i) {
		Bundle args = new Bundle();
		args.putString(NEWS_TITLE_KEY, news.title);
		args.putBoolean(NEWS_FAVOURITE_KEY, favourite);
		args.putInt(NEWS_INT_KEY, i);

		NewsFragment fragment = new NewsFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		FragmentNewsBinding binding = DataBindingUtil.inflate(inflater,
				R.layout.fragment_news, container, false, mComponent);
		mBinding = new AutoClearedValue<>(this, binding);
		return binding.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mViewModel = ViewModelProviders.of(this, mFactory).get(NewsFragmentViewModel.class);
		showUi();
	}

	private void showUi() {
		Bundle args = getArguments();
		NewsPagerAdapter adapter = new NewsPagerAdapter(getChildFragmentManager());
		mBinding.get().viewPager.setAdapter(adapter);
		mViewModel.getNewsList(args.getBoolean(NEWS_FAVOURITE_KEY)).observe(this, resource -> {
			if (resource != null) {
				adapter.setNewsList(resource.data);
				mBinding.get().viewPager.setCurrentItem(args.getInt(NEWS_INT_KEY));
			}
		});
	}

	@Override
	public LifecycleRegistry getLifecycle() {
		return mRegistry;
	}
}