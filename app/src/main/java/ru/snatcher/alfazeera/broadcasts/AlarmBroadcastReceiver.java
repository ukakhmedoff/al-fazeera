package ru.snatcher.alfazeera.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.snatcher.alfazeera.data.services.UpdateDataService;

public class AlarmBroadcastReceiver extends BroadcastReceiver {
	public static final int REQUEST_CODE = 123124521;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("alfazeeraLog", "onReceive: alarm");

		Intent i = new Intent(context, UpdateDataService.class);
		context.startService(i);
	}
}