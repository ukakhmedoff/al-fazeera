package ru.snatcher.alfazeera.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;

import com.facebook.AccessToken;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import ru.snatcher.alfazeera.R;
import ru.snatcher.alfazeera.broadcasts.AlarmBroadcastReceiver;
import ru.snatcher.alfazeera.other.util.Constants;
import ru.snatcher.alfazeera.ui.common.NavigationController;

public class MainActivity extends AppCompatActivity implements LifecycleRegistryOwner,
		HasSupportFragmentInjector {

	private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

	NavigationView mNavigationView;

	@Inject
	DispatchingAndroidInjector<Fragment> mInjector;

	@Inject
	NavigationController mController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		scheduleAlarm();
		initView();

		if (!isLoggedIn()) {
			visibleViews();
			mController.showFragment(Constants.FRAGMENT_SPLASH_ID);
		} else {
			if (savedInstanceState == null) {
				mController.showFragment(Constants.FRAGMENT_FEED_ID);
			}
		}
	}

	public boolean isLoggedIn() {
		AccessToken accessToken = AccessToken.getCurrentAccessToken();
		return accessToken != null || VKSdk.isLoggedIn();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
			@Override
			public void onResult(VKAccessToken res) {
				mController.showFragment(Constants.FRAGMENT_FEED_ID);
			}

			@Override
			public void onError(VKError error) {
			}
		};

		if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}


	public void scheduleAlarm() {
		final Intent intent = new Intent(getApplicationContext(), AlarmBroadcastReceiver.class);

		final PendingIntent pIntent = PendingIntent.getBroadcast(this,
				AlarmBroadcastReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		long firstMillis = System.currentTimeMillis();
		AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
				AlarmManager.INTERVAL_FIFTEEN_MINUTES, pIntent);
	}

	private void visibleViews() {
		mNavigationView.setVisibility(View.GONE);
	}

	private void initView() {
		mController = new NavigationController(this);
		DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
		mNavigationView.setNavigationItemSelectedListener(item -> {
			int id = item.getItemId();
			switch (id) {
				case R.id.nav_main:
					mController.showFragment(Constants.FRAGMENT_FEED_ID);
					drawerLayout.closeDrawer(Gravity.START);
					return true;
				case R.id.nav_favourite:
					mController.showFragment(Constants.FRAGMENT_FAVOURITE_ID);
					drawerLayout.closeDrawer(Gravity.START);
					return true;
				case R.id.nav_dev_info:
					mController.showFragment(Constants.FRAGMENT_INFO_ID);
					drawerLayout.closeDrawer(Gravity.START);
					return true;

			}
			drawerLayout.closeDrawer(Gravity.START);
			return false;
		});
	}

	@Override
	public void onBackPressed() {
		if (mController.getBackStackCount() > 0) {
			mController.back();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public LifecycleRegistry getLifecycle() {
		return mRegistry;
	}

	@Override
	public AndroidInjector<Fragment> supportFragmentInjector() {
		return mInjector;
	}
}