package ru.snatcher.alfazeera.ui.fragments.feed;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ru.snatcher.alfazeera.data.repositories.NewsRepository;
import ru.snatcher.alfazeera.data.vo.News;
import ru.snatcher.alfazeera.data.vo.Resource;

public class FeedFragmentViewModel extends ViewModel {

	private NewsRepository mRepository;

	@Inject
	FeedFragmentViewModel(NewsRepository repository) {
		mRepository = repository;
	}

	LiveData<Resource<List<News>>> getNewsList(boolean favourite, boolean fetch) {
		return mRepository.getNewsList(favourite, fetch);
	}
}